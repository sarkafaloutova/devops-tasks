FROM python:3.9

WORKDIR /opt/devops-task

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY clinic .

COPY docker-entrypoint.sh .

ENTRYPOINT ["sh", "docker-entrypoint.sh"]

CMD [ "gunicorn", "clinic.wsgi" ]